namespace JSAM {
    public enum Sounds {
        cannon_1,
        cannon_2,
        damage,
        dodge_1,
        dodge_2,
        flamethrow,
        lazer,
        minigun,
        sword_damage,
        sword_miss
    }
    public enum Music {
        main_music
    }
}
